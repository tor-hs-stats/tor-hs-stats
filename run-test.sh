#!/usr/bin/env bash

TRACE_PATH="/tmp/tor-trace-experiment"
TRACE_SESSION_NAME="tor-exp"
HS_ONION_ADDR="g7n46aztcqhxiijv.onion"
CHUTNEY_NETWORK="hs"

#BOOTSTRAP_TIME=45
BOOTSTRAP_TIME=3

# Change this to pipe the output of the commands somewhere else than stdout.
# For instance, ">/dev/null 2>&1"
OUTPUT_DST=""

if [ -z $CHUTNEY_HOME ]; then
	# Path to chutney repository top level. Change it accordingly.
	CHUTNEY_HOME="/home/dgoulet/Documents/git/chutney"
fi

if [ -z $TOR_PATH ]; then
	# Full path to tor binary. Change it accordingly.
	TOR_PATH="/home/dgoulet/Documents/git/tor/src/or/tor"
fi

if [ -z $LIB_UST_FORK ]; then
	# If installed using the git, it's usually in /usr/local else it will be in
	# /usr/lib (or whatever path your crazy distro uses :).
	LIB_UST_FORK="/usr/local/lib/liblttng-ust-fork.so"
fi

# By default, this tracing will be repeated 10 times.
nr_iter=10
if [ ! -z "$1" ]; then
	nr_iter=$1
fi

for i in `seq 1 $nr_iter`; do
	echo ""
	echo "Experiment $i starting..."

	# Create trace session
	lttng create $TRACE_SESSION_NAME-$i -U $TRACE_PATH/$TRACE_SESSION_NAME-$i $OUTPUT_DST
	lttng enable-event -s $TRACE_SESSION_NAME-$i -u 'tor_*' $OUTPUT_DST
	lttng start $TRACE_SESSION_NAME-$i $OUTPUT_DST
	echo "  [+] Tracing started"

	# Start chutney
	cd $CHUTNEY_HOME
	LD_PRELOAD=$LIB_UST_FORK CHUTNEY_TOR=$TOR_PATH ./chutney start networks/$CHUTNEY_NETWORK $OUTPUT_DST
	cd -
	echo "  [+] Chutney has been started."

	echo "  [+] Waiting for chutney bootstrap for $BOOTSTRAP_TIME seconds."
	sleep $BOOTSTRAP_TIME

	# Fireup torsocks
	echo "  [+] Launching torsocks to connect to $HS_ONION_ADDR"
	echo "" | TORSOCKS_CONF_FILE="torsocks.conf" torsocks -u $i -p $i nc -q 1 $HS_ONION_ADDR 6000

	# Stop chutney
	cd $CHUTNEY_HOME
	./chutney stop networks/$CHUTNEY_NETWORK $OUTPUT_DST
	cd - 
	echo "  [+] Chutney has been stoped"

	# Stop tracing and run analysis
	lttng stop $TRACE_SESSION_NAME-$i $OUTPUT_DST
	lttng destroy $TRACE_SESSION_NAME-$i $OUTPUT_DST

	./lttng-analyze.py --quiet "$TRACE_PATH/$TRACE_SESSION_NAME-$i" $OUTPUT_DST
	echo "  [+] Analysis done."
	# Cleanup trace
	rm -rf $TRACE_PATH/$TRACE_SESSION_NAME-$i $OUTPUT_DST
done

rmdir $TRACE_PATH $OUTPUT_DST
