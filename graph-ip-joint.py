#!/usr/bin/env python3

# Graph the IP data but with a joint plot:
# http://web.stanford.edu/~mwaskom/software/seaborn/generated/seaborn.jointplot.html
# This adds a bivariate and univariate graphs on the side of the axis to the
# data frequency can be easily visualized.

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib as mpl
import matplotlib.pyplot as plt

data = pd.read_csv("/tmp/tor-ip.csv")

# Create data frame with an index for each entry so we can set the X axis with
# those values.
df = pd.DataFrame({\
        'idx': list(range(len(data.index))),
        # Transform nsec to msec. Take build time which is the full time for an
        # intro point to be established (including circuit build time).
        'build_time': data.get("build_time").divide(1000000)})

x_min = int(round(df.min()['idx']))
x_max = int(round(df.max()['idx']))
y_min = int(round(df.min()['build_time']))
y_max = int(round(df.max()['build_time']))

# Align on a factor of 5 floor for min value and round + 5 for max
x_min = x_min - (x_min % 5)
x_max = (int(x_max / 5) * 5) + 5
y_min = y_min - (y_min % 5)
y_max = (int(y_max / 5) * 5) + 5

x_range = (x_min, x_max)
y_range = (y_min, y_max)

plt.title("Rendezvous Point Establishing Time")

p = sns.jointplot(x = "idx", y = "build_time", data = df, \
        size=10, xlim = x_range, ylim = y_range, kind="reg")

plt.xticks(list(range(x_range[0], x_range[1], 25)))
plt.yticks(list(range(y_range[0], y_range[1], 5)))

p.fig.savefig("./ip-joint.png")
