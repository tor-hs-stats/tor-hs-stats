#!/usr/bin/env python3

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib as mpl
import matplotlib.pyplot as plt

data = pd.read_csv("/tmp/tor-rp.csv")

# Create data frame with an index for each entry so we can set the X axis with
# those values.
df = pd.DataFrame({\
        'idx': list(range(len(data.index))),
        # Transform nsec to msec.
        'ready_time': data.get("ready_time").divide(1000000),
        'cannibalized': data.get("cannibalized")})

x_min = int(round(df.min()['idx']))
x_max = int(round(df.max()['idx']))
y_min = int(round(df.min()['ready_time']))
y_max = int(round(df.max()['ready_time']))

# Align on a factor of 5 floor for min value and round + 5 for max
x_min = x_min - (x_min % 5)
x_max = (int(x_max / 5) * 5) + 5
y_min = y_min - (y_min % 5)
y_max = (int(y_max / 5) * 5) + 5

x_range = (x_min, x_max)
y_range = (y_min, y_max)

p = sns.lmplot(x = "idx", y = "ready_time", data = df, \
        hue = "cannibalized", legend=False, size=8, aspect=1.5)
p.set(xlim = x_range, ylim = y_range)
p.set_xlabels("Number of RPs")
p.set_ylabels("Milliseconds")

plt.xticks(list(range(x_range[0], x_range[1], 25)))
plt.yticks(list(range(y_range[0], y_range[1], 5)))
plt.legend(["Non-Cannibalized Circuit", "Cannibalized Cicuit"])
plt.title("Rendezvous Point Establishing Time")

p.savefig("./rp.png")
