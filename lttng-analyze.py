#!/usr/bin/env python3

# Copyright (C) 2014 - David Goulet <dgoulet@ev0ke.net>
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License, version 2 only, as published by
# the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import csv
import os
import sys
import time
import argparse
import statistics

try:
    from babeltrace import TraceCollection
except ImportError:
    # quick fix for debian-based distros
    sys.path.append("/usr/local/lib/python%d.%d/site-packages" %
                    (sys.version_info.major, sys.version_info.minor))
    from babeltrace import TraceCollection

NSEC_PER_SEC = 1000000000

# Taken from or.h thus this is Tor ABI.
V_ESTABLISH_INTRO = 32
V_ESTABLISH_RENDEZVOUS = 33
V_INTRODUCE1 = 34
V_INTRODUCE2 = 35
V_RENDEZVOUS1 = 36
V_RENDEZVOUS2 = 37
V_INTRO_ESTABLISHED = 38
V_RENDEZVOUS_ESTABLISHED = 39
V_INTRODUCE_ACK = 40

RELAY_COMMANDS = {
    V_ESTABLISH_INTRO : "ESTABLISH_INTRO",
    V_ESTABLISH_RENDEZVOUS : "ESTABLISH_RENDEZVOUS",
    V_INTRODUCE1 : "INTRODUCE1",
    V_INTRODUCE2 : "INTRODUCE2",
    V_RENDEZVOUS1 : "RENDEZVOUS1",
    V_RENDEZVOUS2 : "RENDEZVOUS2",
    V_INTRO_ESTABLISHED : "INTRO_ESTABLISHED",
    V_RENDEZVOUS_ESTABLISHED : "RENDEZVOUS_ESTABLISHED",
    V_INTRODUCE_ACK : "INTRODUCE_ACK",
}

# Taken from or.h thus the tor ABI. Circuit purposes.
V_CIRCUIT_PURPOSE_C_GENERAL = 5
V_CIRCUIT_PURPOSE_C_INTRODUCING = 6
V_CIRCUIT_PURPOSE_C_INTRODUCE_ACK_WAIT = 7
V_CIRCUIT_PURPOSE_C_INTRODUCE_ACKED = 8
V_CIRCUIT_PURPOSE_C_ESTABLISH_REND = 9
V_CIRCUIT_PURPOSE_C_REND_READY = 10
V_CIRCUIT_PURPOSE_C_REND_READY_INTRO_ACKED = 11
V_CIRCUIT_PURPOSE_C_REND_JOINED = 12
V_CIRCUIT_PURPOSE_S_ESTABLISH_INTRO = 14
V_CIRCUIT_PURPOSE_S_INTRO = 15
V_CIRCUIT_PURPOSE_S_CONNECT_REND = 16
V_CIRCUIT_PURPOSE_S_REND_JOINED = 17
V_CIRCUIT_PURPOSE_TESTING = 18

CIRCUIT_PURPOSES = {
    V_CIRCUIT_PURPOSE_C_GENERAL : "General-purpose client",
    V_CIRCUIT_PURPOSE_C_INTRODUCING : "HSc: Connecting to intro point",
    V_CIRCUIT_PURPOSE_C_INTRODUCE_ACK_WAIT : "HSc: Waiting for ack from intro point",
    V_CIRCUIT_PURPOSE_C_INTRODUCE_ACKED : "HSc: Received ack from intro point",
    V_CIRCUIT_PURPOSE_C_ESTABLISH_REND : "HSc: Establishing rendezvous point",
    V_CIRCUIT_PURPOSE_C_REND_READY : "HSc: Pending rendezvous point",
    V_CIRCUIT_PURPOSE_C_REND_READY_INTRO_ACKED : "HSc: Pending rendezvous point (ack received",
    V_CIRCUIT_PURPOSE_C_REND_JOINED : "HSc: Active rendezvous point",
    V_CIRCUIT_PURPOSE_S_ESTABLISH_INTRO : "HSs: Establishing introduction point",
    V_CIRCUIT_PURPOSE_S_INTRO : "HSs: Introduction point",
    V_CIRCUIT_PURPOSE_S_CONNECT_REND : "HSs: Connecting to rendezvous point",
    V_CIRCUIT_PURPOSE_S_REND_JOINED : "HSs: Active rendezvous point",
    V_CIRCUIT_PURPOSE_TESTING : "Testing purpose",
}

indent2 = "  "
indent4 = "    "

# CSV writer for stat collection
ip_csv_writer = None
rp_csv_writer = None
hsdir_client_csv_writer = None

args = None

# Global dict of relays. Indexed by nickname.
relays = {}
# Global dict of rendezvous points.
rendezvous_points = {}

def ns_to_hour_nsec(ns):
    d = time.localtime(ns/NSEC_PER_SEC)
    return "%02d:%02d:%02d.%09d" % (d.tm_hour, d.tm_min, d.tm_sec,
            ns % NSEC_PER_SEC)

def PRINT(x):
    if not args.quiet:
        print(x)

class CSVWriter:
    def __init__(self, pathname, field_names):
        self.pathname = pathname
        self.field_names = field_names
        self.f_csv = None

        self.f_csv = open(pathname, 'a+')
        self.writer = csv.DictWriter(self.f_csv, fieldnames=field_names)
        if not os.path.isfile(pathname) or os.path.getsize(pathname) == 0:
            self.writer.writeheader()

    def close(self):
        if self.f_csv:
            self.f_csv.close()

    def record_data(self, data):
        self.writer.writerow(data)

class IntroPointCSVWriter:
    def __init__(self):
        self.fields = ['index', 'nickname', 'circ_build_time',
                       'established_time', 'build_time', 'cannibalized',
                       'exit_nick']
        self.pathname = "/tmp/tor-ip.csv"
        self.writer = CSVWriter(self.pathname, self.fields)
        self.count = 0

    def write(self, nick, e_t, c_b_t, b_t, c, e_n):
        self.count += 1
        data = {self.fields[0] : self.count,
                self.fields[1] : nick,
                self.fields[2] : e_t,
                self.fields[3] : c_b_t,
                self.fields[4] : b_t,
                self.fields[5] : c,
                self.fields[6] : e_n }
        self.writer.record_data(data)

    def close(self):
        self.writer.close()

class RendezvousPointCSVWriter:
    def __init__(self):
        self.fields = ['index', 'nickname', 'circ_build_time',
                       'established_time', 'esta_to_rdv2_time', 'ready_time',
                       'cannibalized']
        self.pathname = "/tmp/tor-rp.csv"
        self.writer = CSVWriter(self.pathname, self.fields)
        self.count = 0

    def write(self, nick, c_b_t, e_t_r_t, e_t, r_t, can):
        self.count += 1
        data = {self.fields[0] : self.count,
                self.fields[1] : nick,
                self.fields[2] : c_b_t,
                self.fields[3] : e_t_r_t,
                self.fields[4] : e_t,
                self.fields[5] : r_t,
                self.fields[6] : can }
        self.writer.record_data(data)

    def close(self):
        self.writer.close()

class HSDirClientCSVWriter:
    def __init__(self):
        self.fields = ['index', 'time', 'action']
        self.pathname = "/tmp/tor-hsdir-client.csv"
        self.writer = CSVWriter(self.pathname, self.fields)
        self.count = 0

    def write(self, t, a):
        self.count += 1
        data = {self.fields[0] : self.count,
                self.fields[1] : t,
                self.fields[2] : a }
        self.writer.record_data(data)

    def close(self):
        self.writer.close()

class Cell:
    def __init__(self, cmd, start_ts):
        self.cmd = cmd
        self.start_ts = start_ts
        self.done_ts = 0
        self.err = 0

    def to_str(self):
        return RELAY_COMMANDS[self.cmd]

    def get_delta_ts(self):
        if self.done_ts != 0:
            return self.done_ts - self.start_ts
        return 0

    def print_info(self, indent):
        PRINT("%s%s cell lasted %d" % \
                (indent, RELAY_COMMANDS[self.cmd], self.get_delta_ts()))

class Circuit:
    def __init__(self, purpose, flags):
        self.purpose = purpose
        self.flags = flags

        self.circ_id = -1
        self.cannibalized = 0
        self.num_hops = 0
        self.err_reason = 0

        # Circuit establish timestamp.
        self.establish_start_ts = 0
        self.establish_done_ts = 0
        # Circuit is opened and ready timestamp.
        self.has_opened_ts = 0

        # Circuit cannibalized at timestamp.
        self.cannibalized = 0
        self.cannibalized_ts = 0
        self.cannibalized_has_opened_ts = 0
        self.cannibalized_purpose = None
        self.exit_nick = None
        self.cells = []

        # Pending cell on that circuit.
        self.pending_cell = None

    def new_cell(self, val, timestamp):
        self.pending_cell = Cell(val, timestamp)

    def add_pending_cell(self):
        self.cells.append(self.pending_cell)

    def add_cell(self, val, timestamp):
        self.cells.append(Cell(val, timestamp))

    def get_establish_time(self):
        return self.establish_done_ts - self.establish_start_ts

    def get_build_time(self):
        return self.has_opened_ts - self.establish_start_ts

    def get_cannibalized_time(self):
        return self.cannibalized_has_opened_ts - self.cannibalized_ts

    def find_last_cell(self, val):
        """ Return last cell of command value val. """
        for cell in self.cells:
            if cell.cmd == val:
                return cell
        return None

    def print_events(self, indent):
        PRINT("%sCircuit %d established in %d with build time %d" % \
                (indent, self.circ_id, self.get_establish_time(),
                    self.get_build_time()))
        if self.cannibalized:
            PRINT("%s%sCannibalized in %d" % \
                    (indent, indent2, self.get_cannibalized_time()))
        PRINT("%s%sPurpose: %s" % (indent, indent2, CIRCUIT_PURPOSES[self.purpose]))
        for cell in self.cells:
            PRINT("%s%s- %s processed in %d" % (indent, indent2, \
                    cell.to_str(),
                    cell.get_delta_ts()))
        if self.err_reason != 0:
            PRINT("%s%sclosed with reason %d" % \
                    (indent, indent2, self.err_reason))

class IntroPoint:
    def __init__(self, service_id, exit_nick):
        self.service_id = service_id
        self.exit_nick = exit_nick

        self.circ_id = 0
        self.start_ts = 0
        self.done_ts = 0
        self.intro_established_cell = None
        self.establish_intro_seen_ts = 0
        self.intro_circ_cannibalized = 0
        self.failure = 0

    def get_establish_time(self):
        return self.done_ts - self.start_ts

    def get_intro_established_build_time(self):
        return self.intro_established_cell.done_ts - self.start_ts

    def print_info(self, indent, circ, nickname):
        circ.print_events(indent + indent4)
        if self.failure:
            PRINT("%sFailure type: %s" % (indent4, ip.failure))

        if not self.intro_established_cell:
            PRINT("%s%sdid not established." % (indent, indent4))
            return

        # What's important here is the time pass either building or
        # cannibalizing the circuit we needed for the  intro point.
        if circ.cannibalized:
            circ_build_time = circ.get_cannibalized_time()
        else:
            circ_build_time = circ.get_build_time()

        PRINT("%s%sEstablished in %d" % (indent, indent4, \
                self.get_intro_established_build_time()))
        ip_csv_writer.write(nickname, self.get_establish_time(),
                circ_build_time, self.get_intro_established_build_time(),
                circ.cannibalized, self.exit_nick)

class RendezvousPoint:
    def __init__(self, circ):
        self.start_ts = 0
        self.done_ts = 0
        self.circ = circ
        # RENDEZVOUS2 cell seen on the circuit.
        self.rendezvous2_cell = None
        # RENDEZVOUS_ESTABLISHED cell seen on the circuit.
        self.rdv_established_cell = None

    def set_needed_cells(self):
        self.rendezvous2_cell = self.circ.find_last_cell(V_RENDEZVOUS2)
        self.rdv_established_cell = \
                self.circ.find_last_cell(V_RENDEZVOUS_ESTABLISHED)

    def get_delta_ts(self):
        return self.done_ts - self.start_ts

    def get_establish_time(self):
        """
        Time from the creation of this rendezvous point up to finishing
        handling the RENDEZVOUS_ESTABLISHED cell.
        """
        return self.rdv_established_cell.done_ts - self.start_ts

    def get_ready_time(self):
        """
        Time from the creation of this rendezvous point up to receiving the
        RENDEZVOUS2 cell which indicates that the data is ready for
        transmission.
        """
        if not self.rendezvous2_cell:
            return 0
        return self.rendezvous2_cell.done_ts - self.start_ts

    def get_esta_to_rdv2_time(self):
        """
        Time from the ESTABLISH_RENDEZVOUS cell arrived up to the RENDEZVOUS2
        end of processing cell.
        """
        return self.rendezvous2_cell.done_ts - \
               self.rdv_established_cell.start_ts

    def print_info(self, indent, nickname):
        # Find RENDEZVOUS2 and RENDEZVOUS_ESTABLISHED cells to use them.
        self.set_needed_cells()

        self.circ.print_events(indent + indent4)
        PRINT("%sEstablished in %d" % (indent + indent4,
            self.get_establish_time()))
        PRINT("%sReady for data transmission in %d" % (indent + indent4,
            self.get_ready_time()))

        # What's important here is the time pass either building or
        # cannibalizing the circuit we needed for the rendezvous point
        if self.circ.cannibalized:
            circ_build_time = self.circ.get_cannibalized_time()
        else:
            circ_build_time = self.circ.get_build_time()

        rp_csv_writer.write(nickname, circ_build_time,
                self.get_establish_time(), self.get_esta_to_rdv2_time(),
                self.get_ready_time(), self.circ.cannibalized)

class Relay:
    """
    Represent a relay node identified and indexed with a unique nickname.
    """
    def __init__(self, nickname):
        self.nickname = nickname
        # Circuit built by this relay indexed by circuit identifier.
        self.circuits = {}
        # Dict. of rendezvous points that Relay has. Indexed by circ id.
        self.rps = {}
        # Dict. of intro points of the Relay. Indexed by circ id.
        self.ips = {}
        # Pending circuit. When establishing a circuit, there a bunch of
        # possible event before we get a fully created circuit that can be
        # indexed with its identifier.
        self.pending_circ = None
        # Cell seen with circ_id == 0. Useful for some use cases. Volatile.
        self.circ_id_zero_cell = None
        # Pending IP since we only know the circ id on the done event.
        self.pending_ip = None

        # HS v2 descriptor fetch.
        self.hs_v2_fetch_times = []
        # From the fetch start to the store event.
        self.hs_v2_total_fetch_time = []
        # Mid relay cell (they are set with circ_id 0)
        self.mid_cells = []

    def record_hs_v2_store(self, timestamp):
        # The heuristic here is that we take the last timestamp of the fetch
        # time and use it for the total time.
        self.hs_v2_total_fetch_time.append(
                timestamp - self.hs_v2_fetch_times[-1][0])

    def new_circuit(self, purpose, flags):
        self.pending_circ = Circuit(purpose, flags)
        return self.pending_circ

    def add_pending_circuit(self):
        self.circuits[self.pending_circ.circ_id] = self.pending_circ
        self.pending_circ = None

    def get_circuit(self, circ_id):
        if circ_id not in self.circuits:
            return None
        return self.circuits[circ_id]

    def new_intro_point(self, service_id, exit_nick):
        self.pending_ip = IntroPoint(service_id, exit_nick)
        return self.pending_ip

    def add_pending_ip(self):
        ip = self.pending_ip
        self.ips[ip.circ_id] = ip
        # Set exit nickname to circ_id. OK to replace, we want the last one.
        self.pending_ip = None

    def get_intro_point(self, circ_id):
        return self.ips[circ_id]

    def create_rendezvous(self, circ):
        self.rps[circ.circ_id] = RendezvousPoint(circ)

    def get_rendezvous(self, circ_id):
        if circ_id not in self.rps:
            return None
        return self.rps[circ_id]

    def print_mid_cells(self):
        establish_rdv_ts = 0
        for cell in self.mid_cells:
            cell.print_info(indent4 + "- ")
            if cell.cmd == V_ESTABLISH_RENDEZVOUS:
                establish_rdv_ts = cell.start_ts
            elif cell.cmd == V_RENDEZVOUS1:
                PRINT("%s%sDelta since cell %s: %d" % \
                        (indent4, indent2, RELAY_COMMANDS[V_ESTABLISH_RENDEZVOUS],
                         cell.done_ts - establish_rdv_ts))
                establish_rdv_ts = 0

    def analyze_relay(self):
        if len(self.mid_cells) > 0:
            PRINT("\n[+] Relay mid cells")
            self.print_mid_cells()

        if len(self.circuits) > 0:
            PRINT("\n[+] Relay's circuits")
            for circ in self.circuits.values():
                circ.print_events(indent2)

    def analyze_intro_points(self):
        if len(self.ips) == 0:
            return
        PRINT("\n[+] Introduction Points")
        for ip in self.ips.values():
            PRINT("%s--> [%s] %s for %s and lasted %d" % \
                    (indent2, ns_to_hour_nsec(ip.start_ts), ip.exit_nick,
                     ip.service_id, ip.get_establish_time()))
            ip.print_info(indent2, self.circuits[ip.circ_id], self.nickname)

    def analyze_hsdir(self):
        if len(self.hs_v2_fetch_times) == 0:
            return
        # Compute fetch times from tuple array
        fetch_times = []
        for t in self.hs_v2_fetch_times:
            fetch_times.append(t[1] - t[0])
            hsdir_client_csv_writer.write(fetch_times[-1], "fetch")

        PRINT("\n[+] HSdir fetch/store")
        avg_fetch = statistics.mean(fetch_times)
        PRINT("%sFetch: %d seen (avg: %d, stdev: %d)" % \
                (indent2, len(fetch_times), avg_fetch,
                 (0 if len(fetch_times) < 2 else statistics.stdev(fetch_times))))

        avg_full_fetch = 0
        if len(self.hs_v2_total_fetch_time) > 0:
            avg_full_fetch = statistics.mean(self.hs_v2_total_fetch_time)

        PRINT("%sStore: %d seen (avg: %d, stdev: %d)" % \
                (indent2, len(self.hs_v2_total_fetch_time), avg_full_fetch,
                 (0 if len(self.hs_v2_total_fetch_time) < 2 else \
                         statistics.stdev(self.hs_v2_total_fetch_time))))
        if len(self.hs_v2_total_fetch_time) > 1:
            stdev = statistics.stdev(self.hs_v2_total_fetch_time)
            PRINT("%sstdev: %d" % (indent4, stdev))

        for ts in self.hs_v2_total_fetch_time:
            hsdir_client_csv_writer.write(ts, "store")

    def analyze_rendezvous_points(self):
        if len(self.rps) == 0:
            return
        PRINT("\n[+] Rendezvous points")
        for rp in self.rps.values():
            PRINT("%s--> [%s] %s and lasted %d" % \
                    (indent2, ns_to_hour_nsec(rp.start_ts), rp.circ.exit_nick,
                     rp.get_delta_ts()))
            rp.print_info(indent2, self.nickname)

#
# Helper functions for the TraceParser.
#

def get_create_relay(nickname):
    if nickname not in relays:
        relays[nickname] = Relay(nickname)
    return relays[nickname]

class TraceParser:
    def __init__(self, trace):
        self.trace = trace

    def parse(self):
        # iterate over all the events
        for event in self.trace.events:
            method_name = "handle_%s" % event.name.replace(":", "_").replace("+", "_")
            # call the function to handle each event individually
            if hasattr(TraceParser, method_name):
                # Get relay associated with this event.
                relay = get_create_relay(event["nickname"])
                func = getattr(TraceParser, method_name)
                func(self, event, relay)

    def analyze_relay(self):
        sorted_names = sorted(relays.keys())
        for name in sorted_names:
            relay = relays[name]
            if len(relay.ips) == 0 and len(relay.rps) == 0 and \
                    len(relay.hs_v2_fetch_times) == 0 and \
                    len(relay.mid_cells) == 0:
                continue
            PRINT("\nRelay: %s\n----------" % (relay.nickname))
            relay.analyze_relay()
            relay.analyze_intro_points()
            relay.analyze_hsdir()
            relay.analyze_rendezvous_points()

    def analyze(self):
        self.analyze_relay()

    #
    # Trace events handlers
    #

    def handle_tor_circuit_has_opened(self, event, relay):
        timestamp = event.timestamp
        nickname = event["nickname"]
        circ_id = event["circ_id"]
        purpose = event["purpose"]

        # Circuit is now fully opened, update circuit build time.
        circ = relay.get_circuit(circ_id)
        if circ is None:
            # Possible if the trace was started *after* this circuit was
            # created. At some point we should inform that this event has been
            # discarded.
            return

        if circ.cannibalized:
            circ.cannibalized_has_opened_ts = timestamp
        else:
            circ.has_opened_ts = timestamp

    def handle_tor_circuit_establish_start(self, event, relay):
        timestamp = event.timestamp
        nickname = event["nickname"]
        purpose = event["purpose"]
        flags = event["flags"]
        exit_nick = event["exit_nick"]

        # Update the pending circuit.
        circ = relay.new_circuit(purpose, flags)
        circ.establish_start_ts = timestamp

    def handle_tor_circuit_establish_done(self, event, relay):
        timestamp = event.timestamp
        circ_id = event["circ_id"]
        nickname = event["nickname"]
        exit_nick = event["exit_nick"]
        num_hops = event["num_hops"]
        err_reason = event["err_reason"]

        relay.pending_circ.circ_id = circ_id
        relay.pending_circ.num_hops = num_hops
        relay.pending_circ.exit_nick = exit_nick
        relay.pending_circ.err_reason = err_reason
        relay.pending_circ.establish_done_ts = timestamp

        relay.add_pending_circuit()
        circ = relay.get_circuit(circ_id)
        if circ is None:
            return

        # Circuit is for a rendez-vous point thus create RP object. Rendezvous
        # event will most probably follow this.
        if circ.purpose == V_CIRCUIT_PURPOSE_C_ESTABLISH_REND:
            relay.create_rendezvous(circ)

    def handle_tor_circuit_cannibalized(self, event, relay):
        timestamp = event.timestamp
        cpu_id = event["cpu_id"]
        nickname = event["nickname"]
        extend_nick = event["extend_nick"]
        circ_id = event["circ_id"]
        purpose = event["purpose"]
        num_hops = event["num_hops"]

        circ = relay.get_circuit(circ_id)
        if circ is None:
            return

        circ.purpose = purpose
        circ.num_hops = num_hops
        circ.exit_nick = extend_nick
        circ.cannibalized_ts = timestamp
        circ.cannibalized = 1

        if circ.purpose == V_CIRCUIT_PURPOSE_S_ESTABLISH_INTRO:
            # IP object is always created before cannibalized circuit.
            if self.ip_pending:
                selt.ip_pending.circ = circ

        if circ.purpose == V_CIRCUIT_PURPOSE_C_ESTABLISH_REND:
            rp = relay.create_rendezvous(circ)

    def handle_tor_hs_relay_cell_start(self, event, relay):
        timestamp = event.timestamp
        cpu_id = event["cpu_id"]
        nickname = event["nickname"]
        cell = event["cell"]
        cell_val = event["cell_val"]
        circ_id = event["circ_id"]

        # Those are cells not originating from the OR named nickname so add
        # them to the middle cells queue.
        if circ_id == 0:
            relay.mid_cells.append(Cell(cell_val, timestamp))
            return

        circ = relay.get_circuit(circ_id)
        if circ is None:
            return

        circ.new_cell(cell_val, timestamp)

    def handle_tor_hs_relay_cell_done(self, event, relay):
        timestamp = event.timestamp
        cpu_id = event["cpu_id"]
        nickname = event["nickname"]
        cell = event["cell"]
        cell_val = event["cell_val"]
        circ_id = event["circ_id"]
        ret = event["ret"]

        if circ_id == 0:
            # Always the last cell.
            relay.mid_cells[-1].done_ts = timestamp
            return

        circ = relay.get_circuit(circ_id)
        if circ is None:
            return
        circ.pending_cell.done_ts = timestamp
        circ.pending_cell.err = ret

        if cell_val == V_INTRO_ESTABLISHED:
            ip = relay.get_intro_point(circ_id)
            ip.intro_established_cell = circ.pending_cell

        circ.add_pending_cell()

    def handle_tor_hs_client_establish_rendezvous_start(self, event, relay):
        timestamp = event.timestamp
        cpu_id = event["cpu_id"]
        nickname = event["nickname"]
        circ_id = event["circ_id"]

        rp = relay.get_rendezvous(circ_id)
        if rp is None:
            return
        rp.start_ts = timestamp

    def handle_tor_hs_client_establish_rendezvous_done(self, event, relay):
        timestamp = event.timestamp
        cpu_id = event["cpu_id"]
        nickname = event["nickname"]
        circ_id = event["circ_id"]
        ret = event["ret"]

        rp = relay.get_rendezvous(circ_id)
        if rp is None:
            return
        rp.done_ts = timestamp

    def handle_tor_hs_client_ip_failure(self, event, relay):
        timestamp = event.timestamp
        cpu_id = event["cpu_id"]
        nickname = event["nickname"]
        circ_id = event["circ_id"]
        failure_type = event["failure_type"]

        ip = relay.get_intro_point(circ_id)
        ip.failure = failure_type

    def handle_tor_hs_client_desc_v2_fetch_start(self, event, relay):
        timestamp = event.timestamp
        cpu_id = event["cpu_id"]
        nickname = event["nickname"]
        onion_address = event["onion_address"]

        relay.hs_v2_fetch_times.append((timestamp, 0))

    def handle_tor_hs_client_desc_v2_fetch_done(self, event, relay):
        timestamp = event.timestamp
        cpu_id = event["cpu_id"]
        nickname = event["nickname"]
        onion_address = event["onion_address"]
        allowed_to_fetch = event["allowed_to_fetch"]
        found_in_cache = event["found_in_cache"]

        # Update second element of the last tuple.
        last_time = relay.hs_v2_fetch_times.pop()
        relay.hs_v2_fetch_times.append((last_time[0], timestamp))

    def handle_tor_hs_client_desc_v2_store(self, event, relay):
        timestamp = event.timestamp
        cpu_id = event["cpu_id"]
        nickname = event["nickname"]
        onion_address = event["onion_address"]

        relay.record_hs_v2_store(timestamp)

    def handle_tor_hs_service_note_new_ip(self, event, relay):
        timestamp = event.timestamp
        cpu_id = event["cpu_id"]
        nickname = event["nickname"]
        service_id = event["service_id"]
        n_intro_points_wanted = event["n_intro_points_wanted"]

        # XXX: Not used in the state system yet.

    def handle_tor_hs_service_establish_intro_start(self, event, relay):
        timestamp = event.timestamp
        cpu_id = event["cpu_id"]
        nickname = event["nickname"]
        exit_nick = event["ip_extend_info"]
        service_id = event["service_id"]

        ip = relay.new_intro_point(service_id, exit_nick)
        ip.start_ts = timestamp

    def handle_tor_hs_service_establish_intro_done(self, event, relay):
        timestamp = event.timestamp
        cpu_id = event["cpu_id"]
        nickname = event["nickname"]
        ip_extend_info = event["ip_extend_info"]
        service_id = event["service_id"]
        circ_id = event["circ_id"]
        ret = event["ret"]

        relay.pending_ip.done_ts = timestamp
        relay.pending_ip.circ_id = circ_id
        relay.add_pending_ip()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Trace parser')
    parser.add_argument('path', metavar="<path/to/trace>", help='Trace path')
    parser.add_argument('--quiet', help='Do not output anything', action='store_true', required=False)
    args = parser.parse_args()

    traces = TraceCollection()
    handle = traces.add_traces_recursive(args.path, "ctf")
    if handle is None:
        sys.exit(1)

    ip_csv_writer = IntroPointCSVWriter()
    rp_csv_writer = RendezvousPointCSVWriter()
    hsdir_client_csv_writer = HSDirClientCSVWriter()

    t = TraceParser(traces)
    t.parse()
    t.analyze()

    for h in handle.values():
        traces.remove_trace(h)
    ip_csv_writer.close()
    rp_csv_writer.close()
    hsdir_client_csv_writer.close()
